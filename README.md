# Hi guys! <img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/gif/wave.gif" width="30"/>

My name is **Daniel Pham** (my full Vietnamese name is **Phạm Trung Dũng**) and I'm a DevOps/DevSecOps engineer. I'm from Vietnam, living with my family in Da Nang city.

You can find me on:
<div align="center">
<a href="https://www.facebook.com/DevOpsLite" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/facebook.png" alt="facebook" title="Facebook" width="30"/></a>&nbsp;&nbsp;
<a href="https://www.youtube.com/@devopslite" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/youtube.png" alt="youtube" title="YouTube" width="30"/></a>&nbsp;&nbsp;
<a href="https://x.com/devopslite" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/x.png" alt="x" title="X" width="30"/></a>&nbsp;&nbsp;
<a href="https://www.instagram.com/devopslite" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/instagram.png" alt="instagram" title="Instagram" width="30"/></a>&nbsp;&nbsp;
<a href="https://www.threads.com/@devopslite" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/threads.png" alt="threads" title="Threads" width="30"/></a>&nbsp;&nbsp;
<a href="https://www.pinterest.com/devopslite" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/pinterest.png" alt="pinterest" title="Pinterest" width="30"/></a>&nbsp;&nbsp;
<a href="https://t.me/devopslite" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/telegram.png" alt="telegram" title="Telegram" width="30"/></a>&nbsp;&nbsp;
<a href="https://devopslite.medium.com" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/medium.png" alt="medium" title="Medium" width="30"/></a>&nbsp;&nbsp;
<a href="https://www.linkedin.com/in/phamtrungdung" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/linkedin.png" alt="linkedin" title="Linkedin" width="30"/></a>&nbsp;&nbsp;
<a href="https://github.com/dungpham91" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/github.png" alt="github" title="Github" width="30"/></a>&nbsp;&nbsp;
</div>

<div align="center"><a href="https://www.buymeacoffee.com/devopslite.com" target="_blank"><img src="https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/buy-me-a-coffee.png" alt="buy-me-a-coffee" title="Donate" width="250"/></a></div>

![](https://gitlab.com/dungpham91/dungpham91/-/raw/main/images/sponsor.svg)

## 🔧 Technologies & Tools

![](https://img.shields.io/badge/OS-Linux-informational?style=flat&logo=linux&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/OS-Ubuntu-informational?style=flat&logo=ubuntu&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/OS-CentOS-informational?style=flat&logo=centos&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Editor-IntelliJ_IDEA-informational?style=flat&logo=intellij-idea&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=python&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Shell-Bash-informational?style=flat&logo=gnu-bash&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Docker-informational?style=flat&logo=docker&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Kubernetes-informational?style=flat&logo=kubernetes&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Tools-Helm-informational?style=flat&logo=helm&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Cloud-AWS-informational?style=flat&logo=amazonaws&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Cloud-Azure-informational?style=flat&logo=microsoftazure&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Cloud-GCP-informational?style=flat&logo=googlecloud&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Cloud-Digital_Ocean-informational?style=flat&logo=digitalocean&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/CI/CD-Jenkins-informational?style=flat&logo=jenkins&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/CI/CD-Gitlab%20CI-informational?style=flat&logo=gitlab&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/CI/CD-Github%20Actions-informational?style=flat&logo=githubactions&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/CI/CD-Bitbucket%20Pipelines-informational?style=flat&logo=bitbucket&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/CI/CD-Circle%20CI-informational?style=flat&logo=circleci&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/CI/CD-Argo%20CD-informational?style=flat&logo=argo&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/IaC-Terraform-informational?style=flat&logo=terraform&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/IaC-Terragrunt-informational?style=flat&logo=terraform&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/IaC-CloudFormation-informational?style=flat&logo=amazonaws&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/IaC-Ansible-informational?style=flat&logo=ansible&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Monitoring-Prometheus--Grafana-informational?style=flat&logo=prometheus&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Monitoring-Icinga-informational?style=flat&logo=icinga&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Logging-Splunk-informational?style=flat&logo=splunk&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Logging-Graylog-informational?style=flat&logo=graylog&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Logging-ELK%20Stack-informational?style=flat&logo=elasticstack&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Logging-EFK%20Stack-informational?style=flat&logo=elasticstack&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/SIEM-Wazuh-informational?style=flat&logo=springsecurity&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Observability-New%20Relic-informational?style=flat&logo=newrelic&logoColor=white&color=2bbc8a)

## ✍️ Blog & Writing

As part of my job, I build my website and write my articles on it.
- Blog: [https://devopslite.com](https://devopslite.com)

Some of my current articles:

<!-- BLOG-POST-LIST:START -->
- [How to delete a role in PostgreSQL](https://devopslite.com/how-to-delete-a-role-in-postgresql/)
- [Should use LastPass for password management?](https://devopslite.com/should-use-lastpass-for-password-management/)
- [Error acquiring the state lock](https://devopslite.com/error-acquiring-the-state-lock/)
- [Don’t believe terraform plan for all the thing](https://devopslite.com/dont-believe-terraform-plan-for-all-the-thing/)
<!-- BLOG-POST-LIST:END -->

<!-- Refer: https://github.com/MartinHeinz/MartinHeinz/blob/master/README.md -->